# Terragrunt.hcl Inheritance
include  {
  path = find_in_parent_folders()
}

# Local Source
#terraform {
#  source = "../../modules/rds/main.tf"
#}

# Bitbucket Source
terraform {
  source = "git::ssh://git@bitbucket.org/ppotter1992/terragrunt-modules.git//rds?ref=master"
}

# Inputs
inputs = {
  #Engine options
  region            = "us-west-2"
  engine            = "postgres"
  engine_version    = "11.9"

  #DB instance class
  instance_class    = "db.t3.medium"

  #Storage
  storage_type      = "gp2"
  allocated_storage = 100
  #iops              = 1000
  
  #Connectivity
  vpc_security_group_ids = ["sg-3e584211"]
  db_subnet_group_name = "default"
  publicly_accessible = false

  #Database authentication
  iam_database_authentication_enabled = false
  
  #Additional configuration
  parameter_group_name = "test-postgres11"
  option_group_name = "default:postgres-11"
  backup_retention_period = 7
  maintenance_window = "Sat:00:00-Sat:03:00"
  backup_window      = "03:00-04:00"
  copy_tags_to_snapshot = true

  tags = {
    Owner       = "dba"
    Environment = "it-devops-test"
  }
  
  auto_minor_version_upgrade = false
  allow_major_version_upgrade = false
  deletion_protection = false
  apply_immediately = true
  ca_cert_identifier = "rds-ca-2019"
  skip_final_snapshot = true

  multi_az = false

  #KMS
  storage_encrypted = true
  #kms_key_id
  #Performance Insights
  performance_insights_enabled = true
  performance_insights_retention_period = 7

  identifier = "demoinst3"
  name     = "demodb3"
  username = "testing"
  password = "YourPwdShouldBeLongAndSecure!"
  port     = "3344"

}